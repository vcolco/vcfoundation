# VCFundation

[![CI Status](http://img.shields.io/travis/zhangzhonging/VCFundation.svg?style=flat)](https://travis-ci.org/zhangzhonging/VCFundation)
[![Version](https://img.shields.io/cocoapods/v/VCFundation.svg?style=flat)](http://cocoapods.org/pods/VCFundation)
[![License](https://img.shields.io/cocoapods/l/VCFundation.svg?style=flat)](http://cocoapods.org/pods/VCFundation)
[![Platform](https://img.shields.io/cocoapods/p/VCFundation.svg?style=flat)](http://cocoapods.org/pods/VCFundation)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

VCFundation is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "VCFundation"
```

## Author

zhangzhonging, cq_zhangzhongming@163.com

## License

VCFundation is available under the MIT license. See the LICENSE file for more info.
