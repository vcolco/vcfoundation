//
//  VCHTTPRequestSerializer.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "VCHTTPRequestSerializer.h"

@implementation VCHTTPRequestSerializer

- (id)init
{
    self = [super init];
    if (self) {
        //Mark:设置本地缓存策略
        //完全忽略本地缓存
        //self.cachePolicy = NSURLRequestUseProtocolCachePolicy;
    }
    return self;
}

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(id)parameters
                                     error:(NSError *__autoreleasing *)error
{
    if (!URLString)
    {
        NSLog(@"=========VC ERROR IN Build Request!===== in %s",__FUNCTION__);
        return nil;
    }
    
    NSMutableURLRequest *req = [super requestWithMethod:method URLString:URLString parameters:parameters error:error];
    [req setTimeoutInterval:10];
    [req setCachePolicy:self.cachePolicy];
#ifdef DEBUG
    NSLog(@"*******request url******** > %@",req.URL.absoluteString);
#endif
    return req;
}
@end
