//
//  VCURLCache.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "VCURLCache.h"

#define kCustomURLCacheExpiration @"kVCCustomURLCacheExpiration"

@implementation VCURLCache

+ (instancetype)standardURLCache
{
    static VCURLCache *_standardURLCache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _standardURLCache = [[VCURLCache alloc]
                             initWithMemoryCapacity:(MemoryCapacity * 1024 * 1024)
                             diskCapacity:(DiskCapacity * 1024 * 1024)
                             diskPath:nil];
    });
    return _standardURLCache;
}


- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request
{
    
    NSCachedURLResponse *cachedResponse = [super cachedResponseForRequest:request];
    if (cachedResponse) {
        //NSLog(@"客户端缓存命中....%@",[request.URL absoluteString]);
        if ([cachedResponse.userInfo[kCustomURLCacheExpiration] compare:[[NSDate date] dateByAddingTimeInterval:CustomURLCacheExpirationInterval]] == NSOrderedDescending) {
            [self removeCachedResponseForRequest:request];
            return nil;
        }
        
    }
    return cachedResponse;
}

- (void)storeCachedResponse:(NSCachedURLResponse *)cachedResponse forRequest:(NSURLRequest *)request
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:cachedResponse.userInfo];
    userInfo[kCustomURLCacheExpiration] = [NSDate date];
    
    NSCachedURLResponse *modifiedCachedResponse = [[NSCachedURLResponse alloc] initWithResponse:cachedResponse.response data:cachedResponse.data userInfo:userInfo storagePolicy:cachedResponse.storagePolicy];
    
    [super storeCachedResponse:modifiedCachedResponse forRequest:request];
}


@end
