//
//  NSString+UUID.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/25.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+UUID.h"
#import "GWSSKeychain.h"
#import "NSString+Device.h"

#define KEY_CHAIN_GROUP_NAME    @"com.vcolco.Sdk.keychain"
#define KEY_CHAIN_IDENTIFY      @"uuid"

@implementation NSString (UUID)

+(NSString *)achieveUUIDWithArchives:(BOOL) isArchives
{
    if (isArchives) {
        NSString *uuid = [GWSSKeychain passwordForService:KEY_CHAIN_GROUP_NAME account:KEY_CHAIN_IDENTIFY];
        if (uuid && ![uuid isEqualToString:@""])
            return uuid;
        else
        {
            NSString *new_uuid = [self achieveUUID];
            [GWSSKeychain setPassword: new_uuid
                           forService:KEY_CHAIN_GROUP_NAME account:KEY_CHAIN_IDENTIFY];
            return new_uuid;
        }
    }else
        return [self achieveUUID];
}

+ (void)cleanUUID
{
    [GWSSKeychain deletePasswordForService:KEY_CHAIN_GROUP_NAME account:KEY_CHAIN_IDENTIFY];
}


+(NSString *)achieveUUID
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue]<6.0) {
        return [NSString achieveMacAddress];
    }else{
        
        return [[UIDevice currentDevice].identifierForVendor UUIDString];
    }
}


@end
