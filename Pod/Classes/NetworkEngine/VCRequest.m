//
//  VCRequest.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "VCRequest.h"
#import "VCHTTPRequestSerializer.h"
#import "VCJSONResponseSerializer.h"

@implementation VCRequest

- (id)initWithBaseDomain:(NSString *)domain
{
    self = [super init];
    if (self) {
        
        NSURL * baseUrl = [NSURL URLWithString:domain];
        _operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseUrl];
        VCJSONResponseSerializer *jsonResponseSerializer = [VCJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        VCHTTPRequestSerializer *httpRequestSerializer = [VCHTTPRequestSerializer serializer];
        [_operationManager setRequestSerializer:httpRequestSerializer];
        [_operationManager setResponseSerializer:jsonResponseSerializer];
    }
    return self;
}

- (void)cancelAllOperation
{
    [_operationManager.operationQueue cancelAllOperations];
}

#pragma mark- 设置请求头信息
- (void)addPrepareHeaderField:(NSString *)aheaderField value:(NSString *)aValue
{
    [_operationManager.requestSerializer setValue:aValue
                               forHTTPHeaderField:aheaderField];

}

#pragma mark - 添加contentType
- (void)addContentTypes:(NSString *)contentTypes
{
    NSMutableSet *customContentTypes = [_operationManager.responseSerializer.acceptableContentTypes mutableCopy];
    [customContentTypes addObject:contentTypes];
    _operationManager.responseSerializer.acceptableContentTypes = customContentTypes;
}

#pragma mrak - request method

#pragma mark - 服务器返回结果解析处理
- (void)handleResp:(id)responseObject
           success:(void (^)(id jsonObject))success
           failure:(void (^)(NSString *errorMsg))failure
{
    if (responseObject) {
        if (success) {
            success(responseObject);
        }
    }else
    {
        if (failure) {
            failure(@"数据解析异常");
        }
    }
}

- (void)GET:(NSString *)urlString
 parameters:(NSDictionary *)parameters
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSString *errorMsg))failure
{
    [_operationManager GET:urlString
                    parameters:parameters
                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                           [self handleResp:responseObject success:success failure:failure];
                           
                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                           if (failure && error)
                           {
                               failure([[error userInfo] objectForKey:NSLocalizedDescriptionKey]);
                           }
                       }];
}


- (void)POST:(NSString *)urlString
  parameters:(NSDictionary *)parameters
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSString *errorMsg))failure
{
    [_operationManager POST:urlString
                parameters:parameters
                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                       [self handleResp:responseObject success:success failure:failure];
                       
                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                       if (failure && error)
                       {
                           failure([[error userInfo] objectForKey:NSLocalizedDescriptionKey]);
                       }
                   }];
}

- (void)PUT:(NSString *)URLString
 parameters:(id)parameters
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSString *errorMsg))failure
{
    [_operationManager PUT:URLString
      parameters:parameters
         success:^(AFHTTPRequestOperation *task, id responseObject) {
             [self handleResp:responseObject success:success failure:failure];
         }
         failure:^(AFHTTPRequestOperation *task, NSError *error) {
             if (failure) {
                 failure([[error userInfo] objectForKey:NSLocalizedDescriptionKey]);
             }
         }
     ];
}


- (void)UPDATE:(NSString *)urlString
    parameters:(NSDictionary *)parameters
          data:(NSData *)data
      fileName:(NSString *)fileName
       success:(void (^)(id jsonObject))success
       failure:(void (^)(NSString *errorMsg))failure
{
    [_operationManager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFormData:data name:fileName];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self handleResp:responseObject success:success failure:failure];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error && failure) {
            failure([error.userInfo objectForKey:NSLocalizedDescriptionKey]);
        }
    }];
}

@end
