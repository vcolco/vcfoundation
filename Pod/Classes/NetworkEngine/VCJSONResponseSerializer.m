//
//  VCJSONResponseSerializer.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "VCJSONResponseSerializer.h"

@implementation VCJSONResponseSerializer

- (id)init
{
    self = [super init];
    if (self) {
        NSMutableSet *customContentTypes = [self.acceptableContentTypes mutableCopy];
        [customContentTypes addObject:@"text/html"];
        
        self.acceptableContentTypes = customContentTypes;
    }
    return self;
}


- (id)responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error
{
    //测试环境下打印返回数据
#ifdef DEBUG
    NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
#endif
    return [super responseObjectForResponse:response data:data error:error];
}

@end
