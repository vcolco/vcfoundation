//
//  UIImage+Utils.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/26.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

+ (UIImage *)imageWithRGB:(NSUInteger)color;

+ (UIImage *)imageWithColor:(UIColor *)color;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;


@end
