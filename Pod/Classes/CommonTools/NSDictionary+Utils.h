//
//  NSDictionary+Utils.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/26.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Utils)

-(NSInteger) intValue:(NSString *)key;
-(NSInteger) intValue:(NSString *)key def:(NSInteger)defaultVaule;

-(NSString *) strValue:(NSString *)key;
-(NSString *) strValue:(NSString *)key def:(NSString *)defaultValue;

-(NSArray *) arrayValue:(NSString *)key;


@end
