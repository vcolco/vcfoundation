//
//  NSString+Utils.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/25.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+AES.h"
#import "NSString+MD5.h"
#import "NSString+Base64.h"
#import "NSString+Device.h"
#import "NSString+UUID.h"

@interface NSString (Utils)

#pragma mark - AES加密
+ (NSData*)encrypt_AES256Data:(NSString*)string withKey:(NSString *)key;//将string转成带密码的data
+ (NSString*)decrypt_AES256Data:(NSData*)data withKey:(NSString *)key;  //将带密码的data转成string

+ (NSString *)encryptAES_128_CBC_Data:(const char *)string key:(const char *)key;//CBC模式下得128位加密
+ (NSString *)decryptAES_128_CBC_Data:(const char *)string ley:(const char *)key;//CBC模式下得128位解密

#pragma mark - Device
+ (NSString *)achieveUniqueIdentifier;//获取ios设备唯一标示符
+ (NSString *)UUID;

#pragma mark - App
+ (NSString *) getAppVersion;//获取app对外版本号

#pragma mark - Validate
- (BOOL)isPureInt;                              //判断字符串是否可以转化为数字
- (BOOL)isPhoneNo;                              //判断字符串是否为手机号码
- (BOOL)isEmail;                                //判断是否为邮箱
- (BOOL)isSMSCodeWithNumberOfDigits:(int)num;   //短信验证码校验
- (BOOL) validateIdentityCard;
- (NSString *)md5;

@end
