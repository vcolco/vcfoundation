//
//  NSDictionary+Utils.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/26.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "NSDictionary+Utils.h"

@implementation NSDictionary (Utils)

-(NSInteger) intValue:(NSString *)key
{
    return [self intValue:key def:0];
}

-(NSInteger) intValue:(NSString *)key def:(NSInteger)defaultValue
{
    id value = [self valueForKey:key];
    if ([value isKindOfClass:[NSNumber class]]) {
        return [value intValue];
    } else {
        return defaultValue;
    }
}

-(NSString *) strValue:(NSString *)key
{
    return [self strValue:key def:nil];
}

-(NSString *) strValue:(NSString *)key def:(NSString *)defaultValue
{
    id value = [self valueForKey:key];
    if ([value isKindOfClass:[NSString class]]) {
        return value;
    } else {
        return defaultValue;
    }
}

-(NSArray *) arrayValue:(NSString *)key
{
    id value = [self valueForKey:key];
    if ([value isKindOfClass:[NSArray class]]) {
        return value;
    }
    return nil;
}


@end
