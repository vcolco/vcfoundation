//
//  VCAppDelegate.h
//  VCFundation
//
//  Created by CocoaPods on 05/23/2015.
//  Copyright (c) 2014 zhangzhonging. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
