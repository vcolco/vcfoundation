//
//  NSError+VCRequest.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "NSError+VCRequest.h"

@implementation NSError (VCRequest)

+(NSError *) errorWithResponse:(NSDictionary *)json
{
    if (json) {
        return nil;
    }
    return [NSError errorWithDomain:@"VCOLCO_Data_ERROR" code:-9999 userInfo:nil];
}

@end
