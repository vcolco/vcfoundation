//
//  UIColor+Utils.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/26.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)
+ (UIColor *)colorWithRGB:(NSUInteger)rgb;
@end
