//
//  NSString+MD5.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/25.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

/**
 *  md5 32位 大写 处理
 *
 *  @param string 源字符串
 *
 *  @return 加密结果
 */
- (NSString *)md5_32_uppercasecase:(NSString *) string;

/**
 *  md5 32位 小写 处理
 *
 *  @param string 源字符串
 *
 *  @return 加密结果
 */
- (NSString *)md5_32_lowercase:(NSString *) string;

/**
 *  md5 64位 小写处理
 *
 *  @param string 源字符串
 *
 *  @return 加密结果
 */
- (NSString *)md5_64_uppercasecase:(NSString *) string;

/**
 *  md5 64位 小写处理
 *
 *  @param string 源字符串
 *
 *  @return 加密结果
 */
- (NSString *)md5_64_lowercase:(NSString *) string;


@end
