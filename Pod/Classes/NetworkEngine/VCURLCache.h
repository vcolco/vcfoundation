//
//  VCURLCache.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CustomURLCacheExpirationInterval 216000 //缓存持续时间，秒为单位
#define MemoryCapacity 10                       //缓存 用于内存的容量限制，M为单位
#define DiskCapacity 2000                       //缓存 用于磁盘的容量限制，M为单位

@interface VCURLCache : NSURLCache

+ (instancetype)standardURLCache;

@end
