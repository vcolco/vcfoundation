//
//  NSString+Utils.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/25.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import <UIKit/UIKit.h>
#import "NSString+Utils.h"
#import "VCAESUtility.h"

@implementation NSString (Utils)

#pragma mark - AES
+ (NSData*)encrypt_AES256Data:(NSString*)string withKey:(NSString *)key
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptedData = [data AES256EncryptWithKey:key];
    return encryptedData;
}


+ (NSString*)decrypt_AES256Data:(NSData*)data withKey:(NSString *)key
{
    NSData *decryData = [data AES256DecryptWithKey:key];
    NSString *string = [[NSString alloc] initWithData:decryData encoding:NSUTF8StringEncoding];
    return string;
}

#pragma mark - 128_CBC_AES
+ (NSString *)encryptAES_128_CBC_Data:(const char *)string key:(const char *)key
{
    return [[[VCAESUtility alloc] initWithKey:key] Encryption:string len:(int)strlen(string)];
}

+ (NSString *)decryptAES_128_CBC_Data:(const char *)string ley:(const char *)key
{
    return [[[VCAESUtility alloc] initWithKey:key] Decryption:string len:(int)strlen(string)];
}


#pragma mark - device unique indentifier
+ (NSString *)achieveUniqueIdentifier
{
    NSString *sysVersion = [UIDevice currentDevice].systemVersion;
    CGFloat version = [sysVersion floatValue];
    
    if (version >= 7.0)
        return [NSString achieveUUIDWithArchives:YES];//返回UDID+KeyChain
    else
        return [NSString achieveMacAddress];//返回mac地址信息
    return nil;
}

#pragma mark app version
+ (NSString *) getAppVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleShortVersionString"];

}

#pragma markPureInt
- (BOOL)isPureInt
{
    NSScanner* scan = [NSScanner scannerWithString:self];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

#pragma mark -Validate
- (BOOL)isPhoneNo
{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"^1[0-9]{10}$"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    NSInteger num = [regex numberOfMatchesInString:self
                                           options:0
                                             range:NSMakeRange(0, self.length)];
    return num == 1;

}

- (BOOL)isEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isSMSCodeWithNumberOfDigits:(int)num
{
    NSString *pattern  = [NSString stringWithFormat:@"^[0-9]{%d}$",num];
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:pattern
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    NSInteger rest = [regex numberOfMatchesInString:self
                                           options:0
                                             range:NSMakeRange(0, self.length)];
    return rest == 1;

}

- (NSString *)md5
{
    const char *cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (NSString *)UUID{
    
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef stringRef = CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
    CFRelease(uuidRef);
    NSString *uuidString = [(__bridge NSString *)stringRef lowercaseString];
    CFRelease(stringRef);
    
    return uuidString;
}

- (BOOL) validateIdentityCard
{
    BOOL flag;
    if (self.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:self];
}
@end
