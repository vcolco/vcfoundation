//
//  main.m
//  VCFundation
//
//  Created by zhangzhonging on 05/23/2015.
//  Copyright (c) 2014 zhangzhonging. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VCAppDelegate class]));
    }
}
