//
//  VCMonitoringHandle.h
//  DoorToDoor
//
//  Created by 张忠明 on 15/4/17.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  友盟数据统计
 */
@interface VCMonitoringHandle : NSObject

/** 
 * @brief  启动监控
 */
+ (void)startMonitoring;

@end
