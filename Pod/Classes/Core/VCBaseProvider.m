//
//  VCBaseProvider.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "VCBaseProvider.h"
#import "VCRequest.h"

@interface VCBaseProvider ()

@property(nonatomic,retain)VCRequest *request;

@end

@implementation VCBaseProvider

- (instancetype)init
{
    self = [super init];
    if (self) {
        _request = [[VCRequest alloc] init];
    }
    return self;
}

- (void)cancleAllOperation
{
    [_request cancelAllOperation];
}



@end
