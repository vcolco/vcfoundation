//
//  VCExceptionUpdateService.h
//  DoorToDoor
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^VCExceptionUpdateHandle)(NSString *errorMsg);

@interface VCExceptionUpdateService : NSObject

@property(nonatomic,copy)VCExceptionUpdateHandle exceptionUpdateblock;

+ (VCExceptionUpdateService *)shared;

@end
