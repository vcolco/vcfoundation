//
//  VCExceptionUpdateService.m
//  DoorToDoor
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "VCExceptionUpdateService.h"

@implementation VCExceptionUpdateService


+ (VCExceptionUpdateService *)shared
{
    static VCExceptionUpdateService *service;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        service = [[VCExceptionUpdateService alloc] init];
    });
    return service;
}


@end
