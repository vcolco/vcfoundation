//
//  UIColor+Utils.m
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/26.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

+ (UIColor *)colorWithRGB:(NSUInteger)rgb {
    float r = ((rgb >> 16) & 0xFF)/255.0f;
    float g = ((rgb >> 8) & 0xFF)/255.0f;
    float b = ((rgb) & 0xFF)/255.0f;
    float a = ((rgb >> 24) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

@end
