#
# Be sure to run `pod lib lint VCFundation.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "VCFundation"
  s.version          = "0.1.1"
  s.summary          = "some common features for vcolco"
  s.description      = <<-DESC
                       For work in some common features , abstracted some basic modules , reduce repetitive tasks , thereby improving efficiency.
                       DESC

  s.homepage         = "https://bitbucket.org/vcolco/vcfoundation"
  s.license          = 'MIT'
  s.author           = { "zhangzhonging" => "cq_zhangzhongming@163.com" }
  s.source           = { :git => "https://zhangzhongming_@bitbucket.org/vcolco/vcfoundation.git", :tag => "v0.1.1"}
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  #s.source_files = 'Pod/Classes/**/*'
  #s.resource_bundles = {
  #'VCFundation' => ['Pod/Assets/*.png']
  #}

  #GTMBase64
  s.subspec 'VCBase64' do |base64|
    base64.source_files = 'Pod/Classes/GTMBase64/**/*'
    base64.public_header_files = 'Pod/Classes/GTMBase64/**/*.h'
    base64.requires_arc = false
  end

  #Exception
  s.subspec 'VCExceptionUpdateHandle' do |exc|
    exc.source_files = 'Pod/Classes/UpdateException/**/*'
    exc.public_header_files = 'Pod/Classes/UpdateException/**/*.h'
    exc.requires_arc = false
    exc.dependency 'VCFundation/Service'
  end

  #Common Util Tools
  s.subspec 'CommonTools' do |tools|
    tools.source_files = 'Pod/Classes/CommonTools/**/*'
    tools.public_header_files = 'Pod/Classes/CommonTools/**/*.h'
    tools.frameworks = 'CoreGraphics','CoreText','CoreTelephony','SystemConfiguration'
    tools.dependency 'VCFundation/KeyChain'
    tools.dependency 'VCFundation/VCBase64'
  end

  #NetWorkEngine
  s.subspec 'NetWorkEngine' do |network|
  	network.source_files = 'Pod/Classes/NetworkEngine/**/*'
  	network.public_header_files = 'Pod/Classes/NetworkEngine/**/*.h'
  	network.dependency 'AFNetworking', '~> 2.5.4'
  end
  
  #Core
  s.subspec 'Core' do |core|
    core.source_files = 'Pod/Classes/Core/**/*'
    core.public_header_files = 'Pod/Classes/Core/**/*.h'
    core.dependency 'VCFundation/NetWorkEngine'
  end

  #Service
  s.subspec 'Service' do |service|
    service.source_files = 'Pod/Classes/Service/**/*'
    service.public_header_files = 'Pod/Classes/Service/**/*.h'
  end

  #KeyChain
  s.subspec 'KeyChain' do |keyChain|
    keyChain.source_files  = 'Pod/Classes/KeyChain/**/*'
    keyChain.public_header_files = 'Pod/Classes/KeyChain/**/*.h'
  end
  
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.5.4'
end
