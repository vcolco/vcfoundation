//
//  VCBaseProvider.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VCBaseProvider : NSObject

/**
 *  取消网络请求.
 */
- (void)cancleAllOperation;

@end
