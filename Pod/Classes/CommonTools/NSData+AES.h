//
//  NSData+AES.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/25.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES)

/**
 *   Data数据的aes 256位加密
 *
 *  @param key 钥匙
 *
 *  @return 加密后的数据
 */
- (NSData *)AES256EncryptWithKey:(NSString *)key;

/**
 *  aes数据数据解密
 *
 *  @param key 钥匙
 *
 *  @return 解密后字符串
 */
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
