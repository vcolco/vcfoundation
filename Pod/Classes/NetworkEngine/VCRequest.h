//
//  VCRequest.h
//  UtilsFrameWork
//
//  Created by 张忠明 on 15/5/27.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface VCRequest : NSObject

#pragma mark - property
@property (nonatomic,readonly)AFHTTPRequestOperationManager *operationManager;


#pragma mark - 设置方法
/**
 *  coustom init method.
 *
 *  @param urlString domain
 *
 *  @return VCRequest
 */
- (id)initWithBaseDomain:(NSString *)domain;

/**
 *  add http request headers.
 *
 *  @param name  http request header name
 *  @param value http request header value.
 */
- (void)addPrepareHeaderField:(NSString *)aheaderField value:(NSString *)aValue;

/**
 *  setting reponse content types.
 *
 *  @param contentTypes such as: `text/html`、`application/xml`、`text/xml`、｀application/json｀
 */
- (void)addContentTypes:(NSString *)contentTypes;

#pragma mark - network operation method
/**
 *  @brief cancle network operation.
 */
- (void)cancelAllOperation;

/**
 *  get network request
 *
 *  @param urlString    url
 *  @param parameters   parameters
 *  @param successBlock sucess call back
 *  @param failureBlock failure call back
 */
- (void)GET:(NSString *)urlString
 parameters:(NSDictionary *)parameters
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSString *errorMsg))failure;


/**
 *  POST NETWORK REQUEST
 *
 *  @param urlString  URL
 *  @param parameters request Parameters
 *  @param success    success CallBack
 *  @param failure    failure CallBack
 */
- (void)POST:(NSString *)urlString
  parameters:(NSDictionary *)parameters
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSString *errorMsg))failure;


/**
 *  PUT Request
 *
 *  @param URLString  url
 *  @param parameters request parameters
 *  @param success    sucess  callback
 *  @param failure    failure callback
 */
- (void)PUT:(NSString *)URLString
 parameters:(id)parameters
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSString *errorMsg))failure;


//??? 文件上传方法 ???
/**
 *  上传文件
 *
 *  @param urlString  url
 *  @param parameters 请求参数
 *  @param data       上传数据
 *  @param fileName   文件名称
 *  @param success    成功回调
 *  @param failure    失败回调
 */
- (void)UPDATE:(NSString *)urlString
    parameters:(NSDictionary *)parameters
          data:(NSData *)data
      fileName:(NSString *)fileName
       success:(void (^)(id jsonObject))success
       failure:(void (^)(NSString *errorMsg))failure;

@end
