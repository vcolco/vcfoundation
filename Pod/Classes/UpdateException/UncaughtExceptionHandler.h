//
//  UncaughtExceptionHandler.h
//  UncaughtExceptions
//
//  Created by 张忠明 on 15/4/17.
//  Copyright (c) 2015年 vcolco. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^VCExceptionUpdateHandle)(NSString *errorMsg);

@interface UncaughtExceptionHandler : NSObject{
	BOOL dismissed;
}

@end

void HandleException(NSException *exception);
void SignalHandler(int signal);
void InstallUncaughtExceptionHandler(VCExceptionUpdateHandle updateHandle);
